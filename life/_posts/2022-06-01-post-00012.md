---
layout: post
title: "[넷플릭스/미드] 굿 걸스(Good Girls, 2018~2021) - 다 먹고 살자고..."
toc: true
---


 [드라마 소개]
 이익금 드라마는 평범한 일상을 보내는 세 가휘 '아줌마'의 '일과'로부터 시작합니다. 그렇지만 장면이 전환되면서, 그들은 불현듯 '쓸데없이 예쁜' 복면을 쓰고 슈퍼마켓에 쳐들어가죠. 플러스 여자들에게 도무지 무슨 일이 벌어진 걸까요? 이익 여자들의 얘기, 한도 순차 들어봐야겠습니다. (1화 스포일러 있습니다.)
 

 복면 삼총사의 선도자 베쓰 볼란드는 4남매의 엄마이자 전업 주부입니다. 남편은 자동차가게 사장이지만, 어찌나 된 일인지 집에 돈이 없습니다. 브라질리언 왁싱을 하러 샵에 갔던 그녀는 신용카드가 정지되었다는 사실을 알게 되죠. 그리고 신용카드가 정지된 이유를 알게 됩니다. 그것은 남편의 바람이었죠.
 댁네 뿐만이 아닙니다. 베쓰는 화가 나서 남편의 서재를 부수다가, 집이 파산 상태에 이르렀다는 사실을 알게 됩니다.
 

 

 루비는 아들과 딸을 번번이 벽 명씩 둔 웨이트리스인데, 딸이 몸이 아파 병원엘 다닙니다. 바꿔 말하면 상태가 악화되어 전개 하나는 극히 망가졌고 다른 하나도 나빠지고 있다는 얘길 듣게 되죠. 아낙네 새라를 살리려면 개화 이식을 해줘야 합니다. 내지 한 달에 만 달러가 드는 신약(에쿨리주맙)을 선서 먹여야 합니다. 둘 수입으로는 꿈도 꾸지 못할 약이죠.

 

 애니는 아들(트랜스젠더?)을 테두리 칭호 둔, 싱글맘인데 슈퍼마켓 계산원 일로 단지 생활하고 있죠. 그런 그녀에게 '전 남편(Gregg)'이란 자가 나타나, '양육권 소송'을 준비중이라는 청천벽력같은 소리를 내뱉고 떠납니다. 용비 많은 여자와 결혼한 전 남편이 소송을 건다면, 양육권 소송에서 질 가운데 있습니다. 애니의 인생에서 누구보다 중요한 아들을, 전 남편과 그의 부인이 빼앗아가려 하고 있는 겁니다. 애니로서는 무슨 수를 쓰지 않으면 계집 되는 상황이었죠.
 

 루비가 일하는 가게에 모인 세 성함 절친은, 신세한탄(?)을 하다가 잘(?)만 하면 강도짓을 해도 잡히지 않는다는 애니의 말에 솔깃합니다. 일련번호를 지운 권총도 세 자루가 구비되어 있다는 말에 '그럼 한량 번?'이란 생각으로 머리가 번쩍 뜨입니다. 우스갯소리로 시작된 변리 대화가, 리얼 타임 스토리가 될 줄은 삼총사도 몰랐을 겁니다. 그럼에도 사건은 벌어지고 말죠.

 

 슈퍼마켓을 장악한 세 명목 여자들. 여기서 베쓰는 뜻밖의 재능을 발견합니다.
 

 

 '범죄(협박)에 있어서 탁월한 [역촌동왁싱](https://www.tomisswaxing.com) 재능'이 있는 그녀의 재발견입니다. 덕분에 매니저를 끌고 금고에 가서, 돈을 빼내오게 됩니다. 어설프기 짝이 없는 세 휘 대비 '강도단'이 첫 범죄로 '돈'을 수확한 겁니다.

 

 우와! 현금 이들은 당장에 해결해야 할 돈을 마련한 걸까요?
 

 

 

 뚱뚱보 경비원이 나타나는 바람에 급하게 도망쳐야 했던 세 여자,
 집에 사신 '챙겨 온 돈'을 확인했는데,
 

## WHAT?
 슈퍼 수입이라고는 생각할 핵 없을 정도로 '지나치게' 돈이 많은 겁니다.
 

 이거 과연, 좋아해도 되는 걸까요?
 

 

 게중에 으뜸 골 장상 돌아가는(?) 루비의 말처럼 거기 돈은 '수상한 돈'인 게 분명했죠. 목표했던 3만 달러의 10배를 웃도는 50만 달러를 어떻게든 처리해야 하는 세 여자.
 

 도리어 어쩌죠? 구제불능(?) 애니는 '돈을 처리해야 한다'는 말에 냉큼, 고급 차를 뽑아 아들의 학교로 픽업을 갑니다. 베쓰는 더구나 어떻고요, 남편의 내연녀에게 거액을 쥐어주고 떠나게 합니다. 루비는 딸의 개진 이식수술을 위해 병원에 신청서를 냅니다.

 이전 모든 게, 거액을 훔친 범죄자가 장래 할 일은 아닐텐데, 어쨌든 이들은 돈으로 한풀이를 합니다. 반면에 기쁨은 잠시였습니다. 강도 당시, 매니저가 돈을 미적미적 꺼내자 특성 급한 애니가 몸을 구부려, 수납장 안의 돈을 뭉텅뭉텅 꺼냈는데요, 제때제때 매니저가 애니의 허리춤에 있는 특유의 문신을 봐버리고 만 것이지요.
 

 

 애니가 고사 좋게 차를 뽑고, 아들이 필요로 하던 노트북까지 사주는 사치를 부린 그날 저녁, 애니의 집에 매니저가 찾아 옵니다. 매니저는 돈을 원하는 걸까요? 도리어 그렇다면 노나주면 될텐데, 그가 원하는 건 그게 아니었습니다. 입막음을 위해, 어쩔 수명 없이 매니저의 청을 들어주기로 하는 애니. 다행히 그때, 아들이 집으로 돌아옵니다. 매니저는 다음을 기약하고 집을 떠나죠. 이것도 분명 문제일텐데, 더욱 큰 문제가 이들 셋을 기다리고 있습니다.

 베쓰의 집에 찾아 온 세 이름자 남자.
 벽 남자는 목에 문신을 했고, 극한 남자는 이목 옆에, 더더군다나 테두리 남자는 팔에 문신을 했고, 그리고 이들은 총을 가지고 있습니다. 그 중, 목에 문신을 한 남자(Rio)가 직접 돈을 돌려주라고 말하죠. 이미 돈을 펑펑 쓴 이들에겐 50만 달러가 없습니다. 마땅히 돌려줄 도리가 없었죠.
 

 애니에게 집착(?)하는 매니저를 처리하느라 또 하나의 범죄를 추가하게 되는 세 여자.
 Rio에게 돈을 돌려주기 위해선, Rio가 시키는 '돈세탁'을 해야 합니다.
 

 

 

 그저, 군자금 노신 가난히 살려던 것 뿐이었는데
 세 여자는 졸지에 '범죄의 세계'에 발을 들여놓게 된 것입니다.
 

 날찍 세 여자는 과연, 돈 시름 없는 삶, 총 꾸지람 없는 삶을 은린옥척 요행 있게 될까요?
 

 강도짓에서 뜻밖의 재능을 발견한 베쓰는, 외타 둘을 이끌며 변치 않는 리더쉽을 발휘하는데요. 컵케익의 달인에서 돈세탁의 달인으로 거듭난 베쓰는, 가족을 지키기 위해 모 위험도 두려워하지 않는 강인한 '엄마'의 모습을 보여줍니다. 가다가 어리석고, 이따금 흔들리는, 좌충우돌 두 고인 과연 베쓰와의 견고한 우정을 지키며 나란히 나아가죠.
 

 언젠가 아무개 사건을 터트릴지 모를, 이 예측불가 세 여인의 활약은 4시즌까지 이어집니다.
 

 [등장인물]
 Beth Boland(Christina Hendricks) : 4남매의 엄마이자 농자 주부. 딘(Dean)의 아내. 컵케익과 뜨개질의 달인(?). 슈퍼 복면강도 삼총사의 리더.

 Ruby Hill(Retta) : 남매의 엄마이자 레스토랑 종업원. 여인 새라에게 전개 이윤 수술을 해줘야 함.
 Annie Mars(Mae Whitman) : 베쓰(Beth)의 동생. 슈퍼 계산원. 아들을 끔찍히 사랑하는 싱글맘.
 

 Stan Hill(Reno Wilson) : Ruby의 남편, 경찰을 꿈꾸는 경비원.
 Rio(Manny Montana) : 조폭. 삼총사에게 돈세탁을 맡긴다. Beth에게 감정이 있는 듯?
 Sara Hill(Lidya Jewett) : Ruby의 딸, 산소통을 끌고 다녀야 할 정도로 건강이 좋지 못하다. 밝고 형씨 주장이 강하고 똑똑하다.
 Dean Boland(Matthew Lillard) : Beth의 남편. 볼랜드 모터스의 사장(차 파는 사람)
 Ben Marks(Isaiah Stannard) : 애니(Annie)의 아들.
 

 

 Jane Boland(Everleigh McDonell) : 베쓰(Beth)의 아낙네 1
 Emma Boland(Scarlett Abinante) :  베쓰(Beth)의 아녀자 2
 Danny Boland(Mason Shea Joyce) :  베쓰(Beth)의 자녀 1
 Kenny Boland(Braxton Bjerken) :  베쓰(Beth)의 아드님 2
 Mick(Carlos Aviles) : Rio의 부하
 Harry Hill(Danny Boyd Jr.) : Ruby의 아들
 FBI Agent Jim Turner(James Lesure) : 삼총사를 통해 Rio를 잡으려고 하는 FBI 요원
 Agent Phoebe Donnegan(Lauren Lapkus) : 삼총사를 통해 Rio를 잡으려고 하는(?) FBI 요원 2
 Gregg(Zach Gilford) : 애니(Annie)의 전남편. 식자 왜냐하면 가끔가끔 만난다. And, 부인이 있는데도...

 Boomer; Leslie 'Boomer' Peterson(David Hornsby) : 삼총사가 강도질한 슈퍼마켓 매니저? 삼총사의 범행을 알고 있는 Boomer는 강도사건 뒤로도 세 여자와 계속해서 엮인다. (애니에게 치근덕거리던 그는, 애니 허리에 있던 타투를 기억하고 있다가, 삼총사 중앙 임계 명이 애니인 걸 알고 임자 뒤로 협박한다.)
 

 [감상 포인트]
 가족을 지키기 위해 조직범죄도 마다하지 않겠다고 하는 드라마가 경계 방침 있죠. 과실 드라마보다 우극 먼저(2008) 만들어진 '브레이킹 배드(Breaking Bad)'입니다. 브레이킹 배드에서도 '범죄를 미화한다'는 비판에 대항(?)하기 위한 의도(?)로, 인체에 무해한 마약을 만든다는 설정을 하죠. 도리어 제조품 과정에서 선한 의도가 투영된다고 하더라도 결과적으로 마약이 삶을 파괴한다는 내용이 뒷부분에 나옵니다. 월터의 소범 상대편 제씨가 망가지죠.
 

 익금 드라마에서 '돈세탁'이 설정된 것도, 여러 범죄들 중에 제일 무해하다는 착상 때문은 아닐까 하는 생각을 누 보았습니다. 오히려 자세히 들여다보지 않아서 그렇지, 위조지폐로 인해서 고통을 받는 소시민도 적실히 있을 거란 생각입니다.
 

 자전 드라마는 역 범죄조직에서 좀처럼 빠져 나오지 못하는 베쓰의 모습을 보여주면서, 범죄에 한도 벌 발을 들이면 빠져나오기 힘들다는 '교훈'을 문뜩 던져주는 것도 같습니다. 오히려 브레이킹 배드보다는 더욱 가벼운 터치로 그려진 드라마이고, 베쓰를 비롯한 '엄마들'의 활약상을 주제로 한다는 점때문인지 의도적으로 '가족의 힘'이나 '가족의 가치'가 강조된다는 느낌도 들었습니다.
 

 But 킬링타임용으로 제격이고, 범위 프로그램 play 버튼을 누르면 멈출 수가 없는 '중독성'을 가진 드라마입니다.
 언제 사고만 일으키는 애니에게 왠지 정이 갔던 드라마였습니다.
 

 미국 NBC에서 2018년에 제작하고, 넷플릭스 한국에선 2020년에 공개되었습니다.
 넷플릭스에서 보실 명맥 있습니다.
 

 긴 마크 읽어주셔서 감사드립니다.
 재미있는 드라마 시청이 되시길 바랍니다.
 

 2022.05.07 - [DramaTiQue/American drama] - [미드] 브레이킹 배드(Breaking bad) vs. 빅 씨(the Big C). : 죽음에 대처하는 '선생님들'의 방식

### 'DramaTiQue > American drama' 카테고리의 다른 글
